﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour {
    public float speed;

    private Rigidbody rb;

	// Update is called once per frame
    void Start () {
        rb = GetComponent<Rigidbody>();
    }
	void FixedUpdate () {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(x, 0.0f, z);

        rb.AddForce(movement * speed);
    }
}
