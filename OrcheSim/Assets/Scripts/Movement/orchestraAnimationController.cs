﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orchestraAnimationController : MonoBehaviour {

	public Animator orchestraAnimator;

	public GameObject orchestraController;
	public bool debugPlayAnimation;

	public bool idle;
	public bool play;

	private string animatorName;

	// Strings
	private bool violinSection1;
	private bool violinSection2;
	private bool violaSection;
	private bool celloSection;
	private bool bassSection;
	// Wind
	private bool fluteSection;
	private bool oboeSection;
	private bool clarinetSection;
	private bool hornSection;
	private bool bassoonSection;
	private bool trumpetSection;
	private bool tromboneSection;
	private bool tbaSection;
	// Percussion
	private bool timpaniSection;
	// Filler
	private bool stockSection;

	private List<bool> orchestraSectionListPlay;
	private int sectionPlay;

	// Use this for initialization
	void Start () {
		animatorName = orchestraAnimator.name;
		idle = true;
		play = false;
		determineOrchestraSection ();
//		orchestraSectionListPlay = new List<bool> () {
//			orchestraController.GetComponent<orchestraMovementController> ().violin1Play,
//			orchestraController.GetComponent<orchestraMovementController> ().violin2Play,
//			orchestraController.GetComponent<orchestraMovementController> ().violaPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().celloPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().bassPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().flutePlay,
//			orchestraController.GetComponent<orchestraMovementController> ().oboePlay,
//			orchestraController.GetComponent<orchestraMovementController> ().clarinetPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().hornPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().bassoonPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().trumpetPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().trombonePlay,
//			orchestraController.GetComponent<orchestraMovementController> ().tbaPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().timpaniPlay,
//			orchestraController.GetComponent<orchestraMovementController> ().stockPlay
//		};
	}
	
	// Update is called once per frame
	void Update () {
		debugPlayAnimation = orchestraController.GetComponent<orchestraMovementController> ().debugPlay;
		determineOrchestraSection ();

		if (debugPlayAnimation) {
			playAnimationPlay ();
		} else {
			if (play) {
				playAnimationPlay ();
			} else if (idle) {
				playAnimationIdle ();
			}
		}
	}

	// Ensures that only play or idle is True
	private void determinePlayState () {
		if (play) {
			idle = false;
		} else {
			idle = true;
		}
	}

	// Plays the play animation for the orchestra
	private void playAnimationPlay () {
		orchestraAnimator.Play ("Play");
	}

	// Plays the idle animation for the orchestra
	private void playAnimationIdle () {
		orchestraAnimator.Play ("Idle");
	}

	// Determines what the current Orchestra Player's section is based on their animator name
	private void determineOrchestraSection () {
		// Strings
		if (animatorName.Contains("ViolinS1")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().violin1Play;
		}
		if (animatorName.Contains("ViolinS2")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().violin2Play;
		}
		if (animatorName.Contains("Viola")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().violaPlay;
		}
		if (animatorName.Contains("Cello")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().celloPlay;
		}
		if (animatorName.Contains("Bass")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().bassPlay;
		}
		// Wind
		if (animatorName.Contains("Flute")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().flutePlay;
		}
		if (animatorName.Contains("Oboe")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().oboePlay;
		}
		if (animatorName.Contains("Clarinet")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().clarinetPlay;
		}
		if (animatorName.Contains("Horn")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().hornPlay;
		}
		if (animatorName.Contains("Bassoon")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().bassoonPlay;
		}
		if (animatorName.Contains("Trumpet")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().trumpetPlay;
		}
		if (animatorName.Contains("Trombone")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().trombonePlay;
		}
		if (animatorName.Contains("TBA")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().tbaPlay;
		}
		// Percussion
		if (animatorName.Contains("Timpani")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().timpaniPlay;
		}
		// Conductor
		if (animatorName.Contains("Conductor")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().conductorPlay;
		}
		// Filler
		if (animatorName.Contains("STOCK")) {
			play = orchestraController.GetComponent<orchestraMovementController> ().stockPlay;
		}
		determinePlayState ();
	}

	// Uses the general section play list to determine whether the current member is playing
	private void determineCurrentPlay (int sectionPlay) {
		play = orchestraSectionListPlay [sectionPlay];
		determinePlayState ();
	}
}
