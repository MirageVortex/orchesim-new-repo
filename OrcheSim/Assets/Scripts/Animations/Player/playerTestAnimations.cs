﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class playerTestAnimations : MonoBehaviour
{

    public Animator playerAnims;
    public Animator chairAnims;
    public Transform player;

    string furniture;

    // bool functions for moving
    // From Door
    bool doorToBed;

    // FromDoor/Bed
    bool doorBedToCompMid;
    bool doorBedToComp;
    bool doorBedToPianoMid;
    bool doorBedToPiano;

    // From Comp
    bool compToBed;
    bool compToCompMid;
    bool compToDoor;
    bool compToPianoMid;
    bool compToPiano;

    // From Bed
    bool bedToDoor;

    // From Piano
    bool pianoToBed;
    bool pianoToDoor;
    bool pianoToComp;
    bool pianoToCompMid;
    bool pianoToMid;

    private Vector3 tracker;

    private Vector3 bedPosition = new Vector3(-40, 1, 50);
    private Vector3 doorPosition = new Vector3(-40, 1, 0);
    private Vector3 compPosition = new Vector3(-2, 1, 13);
    private Vector3 pianoPosition = new Vector3(10, 1, 40);
    private Vector3 beforePianoPosition = new Vector3(-40, 1, 40);
    private Vector3 bedDoorCompBetween = new Vector3(-40, 1, 13);
    private Vector3 pianoCompBetween = new Vector3(-2, 1, 40);

    public float speed;
    private float startTime;
    private float journeyLength;


    // Use this for initialization
    void Start()
    {
        // Real game speed (uncomment to use in game)
        speed = 12.5f;

        // For testing purposes (speed up the animations)
        // speed = 50f;

        doorToBed = false;
        doorBedToCompMid = false;
        doorBedToComp = false;
        doorBedToPianoMid = false;
        doorBedToPiano = false;
        compToBed = false;
        compToCompMid = false;
        compToDoor = false;
        compToPianoMid = false;
        compToPiano = false;
        bedToDoor = false;
        pianoToBed = false;
        pianoToDoor = false;
        pianoToComp = false;
        pianoToCompMid = false;
        pianoToMid = false;

    }

    // Update is called once per frame
    void Update()
    {
        tracker = new Vector3(Mathf.Round(player.position.x), Mathf.Round(player.position.y), Mathf.Round(player.position.z));
        if (doorToBed)
        {
            player.LookAt(bedPosition);
            player.position = Vector3.MoveTowards(player.position, bedPosition, speed * Time.deltaTime);
            if (tracker == bedPosition)
            {
                playerAnims.Play("Armature|Stand_To_Bed");
                doorToBed = false;
            }
        }

        if (doorBedToCompMid)
        {
            player.LookAt(bedDoorCompBetween);
            player.position = Vector3.MoveTowards(player.position, bedDoorCompBetween, speed * Time.deltaTime);
            if (tracker == bedDoorCompBetween)
            {
                player.LookAt(compPosition);
                doorBedToComp = true;
                doorBedToCompMid = false;
            }
        }

        if (doorBedToComp)
        {
            player.position = Vector3.MoveTowards(player.position, compPosition, speed * Time.deltaTime);
            if (tracker == compPosition)
            {
                playerAnims.Play("Armature|Stand_To_Chair");
                chairAnims.Play("Armature|chairAnimate1");
                doorBedToComp = false;
            }
        }

        if (doorBedToPianoMid)
        {
            player.LookAt(beforePianoPosition);
            player.position = Vector3.MoveTowards(player.position, beforePianoPosition, speed * Time.deltaTime);
            if (tracker == beforePianoPosition)
            {
                doorBedToPianoMid = false;
                doorBedToPiano = true;
            }
        }

        if (doorBedToPiano)
        {
            player.LookAt(pianoPosition);
            player.position = Vector3.MoveTowards(player.position, pianoPosition, speed * Time.deltaTime);
            if (tracker == pianoPosition)
            {
                playerAnims.Play("Armature|Stand_To_Piano");
                doorBedToPiano = false;
            }
        }

        if (compToBed)
        {
            player.LookAt(bedPosition);
            player.position = Vector3.MoveTowards(player.position, bedPosition, speed * Time.deltaTime);
            if (tracker == bedPosition)
            {
                playerAnims.Play("Armature|Stand_To_Bed");
                compToBed = false;
            }
        }

        if (compToCompMid)
        {
            player.position = Vector3.MoveTowards(player.position, bedDoorCompBetween, speed * Time.deltaTime);
            player.LookAt(bedDoorCompBetween);
            if (tracker == bedDoorCompBetween)
            {
                if (furniture == "Door")
                {
                    compToDoor = true;
                }
                else
                {
                    compToBed = true;
                }
                compToCompMid = false;
            }
        }

        if (compToDoor)
        {
            player.LookAt(doorPosition);
            player.position = Vector3.MoveTowards(player.position, doorPosition, speed * Time.deltaTime);
            if (tracker == doorPosition)
            {
                playerAnims.Play("Armature|Stand_To_Idle");
                compToDoor = false;
            }
        }

        if (compToPianoMid)
        {
            player.LookAt(pianoCompBetween);
            player.position = Vector3.MoveTowards(player.position, pianoCompBetween, speed * Time.deltaTime);
            if (tracker == pianoCompBetween)
            {
                compToPianoMid = false;
                compToPiano = true;
            }
        }

        if (compToPiano)
        {
            player.LookAt(pianoPosition);
            player.position = Vector3.MoveTowards(player.position, pianoPosition, speed * Time.deltaTime);
            if (tracker == pianoPosition)
            {
                playerAnims.Play("Armature|Stand_To_Piano");
                compToPiano = false;
            }
        }


        if (bedToDoor)
        {
            player.LookAt(doorPosition);
            player.position = Vector3.MoveTowards(player.position, doorPosition, speed * Time.deltaTime);
            if (tracker == doorPosition)
            {
                playerAnims.Play("Armature|Stand_To_Idle");
                bedToDoor = false;
            }
        }

        if (pianoToBed)
        {
            player.LookAt(bedPosition);
            player.position = Vector3.MoveTowards(player.position, bedPosition, speed * Time.deltaTime);
            if (tracker == bedPosition)
            {
                playerAnims.Play("Armature|Stand_To_Bed");
                pianoToBed = false;
            }
        }

        if (pianoToDoor)
        {
            player.LookAt(doorPosition);
            player.position = Vector3.MoveTowards(player.position, doorPosition, speed * Time.deltaTime);
            if (tracker == doorPosition)
            {
                playerAnims.Play("Armature|Stand_To_Idle");
                pianoToDoor = false;
            }
        }

        if (pianoToComp)
        {
            player.LookAt(compPosition);
            player.position = Vector3.MoveTowards(player.position, compPosition, speed * Time.deltaTime);
            if (tracker == compPosition)
            {
                player.LookAt(new Vector3(0, 1, 13));
                playerAnims.Play("Armature|Stand_To_Chair");
                chairAnims.Play("Armature|chairAnimate1");
                pianoToComp = false;
            }
        }

        if (pianoToCompMid)
        {
            player.LookAt(pianoCompBetween);
            player.position = Vector3.MoveTowards(player.position, pianoCompBetween, speed * Time.deltaTime);
            if (tracker == pianoCompBetween)
            {
                pianoToCompMid = false;
                pianoToComp = true;
            }
        }

        if (pianoToMid)
        {
            player.LookAt(beforePianoPosition);
            player.position = Vector3.MoveTowards(player.position, beforePianoPosition, speed * Time.deltaTime);
            if (tracker == beforePianoPosition)
            {
                if (furniture == "Door")
                {
                    pianoToDoor = true;
                }
                else
                {
                    pianoToBed = true;
                }
                pianoToMid = false;
            }
        }
    }

    private void OnMouseDown()
    {
        furniture = gameObject.name;
        // print(furniture);
        if ((tracker == bedPosition && furniture != "Bed") || (tracker == compPosition && furniture != "Computer") || (tracker == doorPosition && furniture != "Door") || (tracker == pianoPosition && furniture != "Piano")) {
            playerAnims.Play("Armature|Walk_Cycle");
            if (furniture == "Bed") {
                if (tracker == doorPosition) {
                    doorToBed = true;
                } else if (tracker == compPosition) {
                    playerAnims.Play("Armature|Chair_To_Stand");
                    chairAnims.Play("Armature|chairAnimate2");
                    Invoke("setCompToCompMid", 3);
                } else if (tracker == pianoPosition) {
                    playerAnims.Play("Armature|Piano_To_Stand");
                    Invoke("setPianoToMid", 5);
                }
            } else if (furniture == "Door") {
                if (tracker == bedPosition) {
                    playerAnims.Play("Armature|Bed_To_Stand");
                    Invoke("setBedToDoor", 6);
                } else if (tracker == compPosition) {
                    playerAnims.Play("Armature|Chair_To_Stand");
                    chairAnims.Play("Armature|chairAnimate2");
                    Invoke("setCompToCompMid", 3);
                } else if (tracker == pianoPosition) {
                    playerAnims.Play("Armature|Piano_To_Stand");
                    Invoke("setPianoToMid", 5);
                }
            } else if (furniture == "Computer") {
                if (tracker == bedPosition) {
                    playerAnims.Play("Armature|Bed_To_Stand");
                    Invoke("setBedToComp", 6);             
                } else if (tracker == doorPosition) {
                    doorBedToCompMid = true;
                } else if (tracker == pianoPosition) {
                    playerAnims.Play("Armature|Piano_To_Stand");
                    Invoke("setPianoToComp", 5);
                }
            } else if (furniture == "Piano") {
                if (tracker == doorPosition) {
                    doorBedToPianoMid = true;
                } else if (tracker == bedPosition) {
                    playerAnims.Play("Armature|Bed_To_Stand");
                    Invoke("setBedToPiano", 6);
                } else if (tracker == compPosition) {
                    playerAnims.Play("Armature|Chair_To_Stand");
                    chairAnims.Play("Armature|chairAnimate2");
                    Invoke("setCompToPianoMid", 3);
                }
            }
        }
    }

    void setCompToCompMid() {
        playerAnims.Play("Armature|Walk_Cycle");
        compToCompMid = true;
    }

    void setCompToPianoMid() {
        playerAnims.Play("Armature|Walk_Cycle");
        compToPianoMid = true;
    }

    void setBedToPiano() {
        playerAnims.Play("Armature|Walk_Cycle");
        doorBedToPianoMid = true;
    }
    void setBedToComp() {
        playerAnims.Play("Armature|Walk_Cycle");
        doorBedToCompMid = true;
    }

    void setBedToDoor() {
        playerAnims.Play("Armature|Walk_Cycle");
        bedToDoor = true;
    }

    void setPianoToComp() {
        playerAnims.Play("Armature|Walk_Cycle");
        pianoToCompMid = true;
    }

    void setPianoToMid() {
        playerAnims.Play("Armature|Walk_Cycle");
        pianoToMid = true;
    }
}
