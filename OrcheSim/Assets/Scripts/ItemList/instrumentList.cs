﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instrumentList : MonoBehaviour {

    //public string[] InstrumentList = { "Flute", "Piccolo", "Alto_Flute", "Oboe", "English_Horn",
    //                                    "B♭_Clarinet", "E♭_Clarinet", "A_Clarinet", "AltoSaxophone", "Tenor_Saxophone",
    //                                    "Baritone_Saxophone", "Bassoon", "Contrabassoon", "Horn_1", "Horn_2",
    //                                    "Horn_3", "Horn_4", "B♭_Trumpet", "C_Trumpet", "Trombone",
    //                                    "Bass_Trombone", "Baritone_Horn", "Tuba", "Violin", "Viola",
    //                                    "Cello", "Double_Bass", "Timpani", "Mallets", "Piano"}; // TOTAL = 30

    public string[] InstrumentList = { "Flute", "Piano", "Violin", "Horn_2", "Double_Bass", "Cello", "Viola" };

    public string[] GenderList = { "Male", "Female" };
}
