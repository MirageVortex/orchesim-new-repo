﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class example : MonoBehaviour {


	// Save System Persistence
	void Awake () 
	{
		// DontDestroyOnLoad ();
	}


	// Save Game function
	public static void saveGame ()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Open (Application.persistentDataPath + "/save.txt", FileMode.Open);
		file.Close ();
	}


	// Load Game function
	void loadGame ()
	{
		if (File.Exists(Application.persistentDataPath + "/save.txt")) 
			{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/save.txt", FileMode.Open);
			file.Close ();
			}
	}
}
