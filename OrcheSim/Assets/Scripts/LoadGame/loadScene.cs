﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class loadScene : MonoBehaviour {

	
	// Update is called once per frame
    public void LoadNewGame() {
        SceneManager.LoadScene("charaCreate");
    }

	public void LoadLevel () {
        SceneManager.LoadScene("playerHome");
	}

    public void LoadOptions()
    {
        SceneManager.LoadScene("optionscene");
    }

    public void LoadTitleScreen()
    {
        SceneManager.LoadScene("titlescreen");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
