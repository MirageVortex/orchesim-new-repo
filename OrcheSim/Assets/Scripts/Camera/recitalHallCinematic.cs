﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class recitalHallCinematic : MonoBehaviour {

	public Camera introCamera;
	public Camera staticCenterCamera;
	public Camera panCamera1;
	public Camera panCamera2;
	public Camera panCamera3;
	public Camera panCamera4;

	private Camera currentCamera;
	private Camera lastUsedCamera;

	private int lastUsedIndex;

	private int time;

	private bool alreadySwitched;

	private System.Action[] cameraSwitchFunctions;

	public string scriptedCameraMovements;
	private int interpretCameraScriptIndex;
	private bool cameraScriptExistence;

	// Ensures camera scripts are clear on game start up
	void Awake () {
		scriptedCameraMovements = null;
	}

	// Initialization of time and Cameras
	void Start () {
		time = 0;
		interpretCameraScriptIndex = 0;
		lastUsedIndex = 0;
		introCamera.enabled = true;
		currentCamera = introCamera;
		staticCenterCamera.enabled = false;
		panCamera1.enabled = false;
		panCamera2.enabled = false;
		panCamera3.enabled = false;
		panCamera4.enabled = false;
		cameraSwitchFunctions = new System.Action[] {switchToStaticCenter, switchToPanCamera1, switchToPanCamera2, switchToPanCamera3, switchToPanCamera4};

		cameraScriptExistence = false;
	}
	
	// Update is called once per frame
	void Update () {
		time = (int)Mathf.Floor (Time.timeSinceLevelLoad);
		if (cameraScriptExistence == true) {
			interpretCameraScript ();
		} else {
			randomCameraSwitch ();
		}
	}



	// 2 Types of Camera switching -- Random and Interpretation
	private void randomCameraSwitch () {
		if (time >= 10 && time % 10 == 0 && alreadySwitched == false) {
			determineCameraRandom ();
			alreadySwitched = true;
		}
		if (time % 10 != 0 && alreadySwitched == true) {
			alreadySwitched = false;
		}
	}

	private void interpretCameraScript () {
		if (time >= 10 && time % 10 == 0 && alreadySwitched == false) {
			determineCameraInterpret ();
			alreadySwitched = true;
		}
		if (time % 10 != 0 && alreadySwitched == true) {
			alreadySwitched = false;
		}
	}
		


	// Used for RANDOM Camera switching
	private void determineCameraRandom () {
		while (true) {
			int index = Random.Range (0, cameraSwitchFunctions.Length);
			print (index + " " + lastUsedIndex);
			if (index != lastUsedIndex) {
				lastUsedIndex = index;
				cameraSwitchFunctions [index] ();
				break;
			} else {
				continue;
			}
		}
	}

	// Used for INTERPRET Camera switching
	private void determineCameraInterpret () {
		cameraSwitchFunctions [int.Parse (scriptedCameraMovements [interpretCameraScriptIndex].ToString ())] ();
		interpretCameraScriptIndex++;
	}



	// Camera Switch functions
	private void switchToIntro () {
		introCamera.enabled = true;
		currentCamera.enabled = false;
		currentCamera = lastUsedCamera;
		currentCamera = introCamera;
	}

	private void switchToStaticCenter () {
		staticCenterCamera.enabled = true;
		currentCamera.enabled = false;
		currentCamera = lastUsedCamera;
		currentCamera = staticCenterCamera;
	}

	private void switchToPanCamera1 () {
		panCamera1.enabled = true;
		currentCamera.enabled = false;
		currentCamera = lastUsedCamera;
		currentCamera = panCamera1;
	}

	private void switchToPanCamera2 () {
		panCamera2.enabled = true;
		currentCamera.enabled = false;
		currentCamera = lastUsedCamera;
		currentCamera = panCamera2;
	}

	private void switchToPanCamera3 () {
		panCamera3.enabled = true;
		currentCamera.enabled = false;
		currentCamera = lastUsedCamera;
		currentCamera = panCamera3;
	}

	private void switchToPanCamera4 () {
		panCamera4.enabled = true;
		currentCamera.enabled = false;
		currentCamera = lastUsedCamera;
		currentCamera = panCamera4;
	}
}
