﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class overworldCamera : MonoBehaviour {

    public GameObject enterbutton;
    public float maxspeed;
    private float rspeed;
    private float uspeed;
    private float right;
    private float up;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        right = ((Screen.width - Input.mousePosition.x) / Screen.width) * 2.0f - 1.0f;
        if (right > 0.8f || right < -0.8f)
        {
            enterbutton.SetActive(false);
            if (Mathf.Abs(rspeed) < Mathf.Abs(maxspeed*((Mathf.Abs(right)-0.8f)*5)))
                rspeed += right;

        } else
            rspeed *= 0.5f;
        if ((transform.position.x < 56 && rspeed<0) || (transform.position.x>30 && rspeed>0))
            transform.position += Vector3.left * rspeed * Time.deltaTime;
        up = ((Screen.height - Input.mousePosition.y) / Screen.height) * 2.0f - 1.0f;
        if (up > 0.8f || up < -0.8f)
        {
            enterbutton.SetActive(false);

            if (Mathf.Abs(uspeed) < Mathf.Abs(maxspeed*((Mathf.Abs(up)-0.8f)*5)))
                uspeed += up;

        }
        else
            uspeed *= 0.5f;
        if ((transform.position.z < -56 && uspeed<0) || (transform.position.z > -92 && uspeed>0))

            transform.position += Vector3.back * uspeed * Time.deltaTime;

    }
}
