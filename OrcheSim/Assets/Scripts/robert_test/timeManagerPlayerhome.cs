﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;



public class timeManagerPlayerhome : MonoBehaviour {

    public GameObject[] lights;
    Vector3 today;
    public Text timetext;
    public Text daytext;
    public Image pointer;
    private string[] tod = { "Morning", "Afternoon", "Evening", "Night" };
    private string[] month = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    private Color[] col = { new Color(0.9411765f, 0.7058824f, 0.9137255f), Color.white, new Color(0.8f, 0.372549f, 0.372549f), new Color(0.1215686f, 0.1254902f, 0.6313726f) };
    private float[] positions = { 39.0f, 96.0f, 154.0f, 210.0f };

    // Use this for initialization
    void Start () {
        lights[0].SetActive(false);
        lights[globalVariables.time].SetActive(true);
        globalVariables.SetOriginalDate(System.DateTime.Today.Year, System.DateTime.Today.Month, System.DateTime.Today.Day);
        UpdateTime();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void NextDay()
    {
        if (globalVariables.time == 3)
        {
            globalVariables.time = 0;
            if (globalVariables.importantEventsDict.Count == 0)
            {
                globalVariables.day++;
            } else
            {
                int dayholder = globalVariables.day;
                while (dayholder!=globalVariables.day && globalVariables.importantEventsDict.Count != 0) { 
                    int newday = globalVariables.importantEventsDict.Keys.Min();
                    if (newday <= globalVariables.day)
                    {
                        globalVariables.importantEventsDict.Remove(newday);
                    } else
                    {
                        globalVariables.day = newday;

                    }
                }
                if (globalVariables.importantEventsDict.Count == 0)
                {
                    globalVariables.day++;
                }

            }
            UpdateTime();
            lights[3].SetActive(false);
            lights[0].SetActive(true);
            globalVariables.daystart = false;
        }
    }
    public void UpdateTime()
    {
        today = globalVariables.GetDateFromInt(globalVariables.day);
        timetext.text = tod[globalVariables.time];
        daytext.text = month[(int)today.y - 1] + " " + (int)today.x + ", " + (int)today.z;
        pointer.rectTransform.position = new Vector3(positions[globalVariables.time], pointer.rectTransform.position.y, 0);
        RenderSettings.ambientLight = col[globalVariables.time];
    }
}
