﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using System.Linq;


public static class globalVariables {

    public static int time;
    public static int day;
    public static bool daystart;
    public static Dictionary<int, int> importantEventsDict;
    public static DateTime dt;
    private static bool datesetup;

    public static void SetOriginalDate(int y, int m, int d)
    {
        if (datesetup ==false) { 
        dt = new DateTime(y, m, d);
        importantEventsDict= new Dictionary<int, int>();
            datesetup = true;

        }
    }

    public static Vector3 GetDateFromInt(double d)
    {
        Vector3 date;
        DateTime newdt = dt.AddDays((d));
        date.x = newdt.Day;
        date.y = newdt.Month;
        date.z = newdt.Year;
        return date;
    }
}
