﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class radarChartPositionModifier : MonoBehaviour {

	public int stat1;
	public int stat2;
	public int stat3;
	public int stat4;
	public int stat5;

	public GameObject origin;
	public GameObject V1;
	public GameObject V2;
	public GameObject V3;
	public GameObject V4;
	public GameObject V5;

	private int[] statList;
	private GameObject[] vList;



	void Start () {
		statList = new int[] { stat1, stat2, stat3, stat4, stat5 };
		vList = new GameObject[] { origin, V1, V2, V3, V4, V5 };
		for (int i = 1 ; i < vList.Length ; i++) {
			vList [i].transform.localPosition = new Vector3 (0, statList[i - 1], 0);
		}
	}
}