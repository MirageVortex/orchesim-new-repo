﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class charaCreate : MonoBehaviour {

	public int instrument;
	public int gender;
	public int age;

	public string firstName;
    public string lastName;
	public string instrumentType;

	public Toggle genderBoolMale;
	public Toggle genderBoolFemale;

	public GameObject instrumentList;
    public GameObject statsFunct;

    private string[] instruments;

	public InputField firstNameInput;
    public InputField lastNameInput;

    public Text instrumentText;

	public playerClass.Player player;

	public List <playerClass.Player> playerList = new List<playerClass.Player>();


	// Use this for initialization
	void Start () {

		instrument = 0;
		gender = 0;
		age = 23;
		instruments = instrumentList.GetComponent<itemList> ().InstrumentList;

        genderBoolMale.isOn = true;
        genderBoolFemale.isOn = false;
	}

    // Constant Update
	void Update () {
		//Debug.Log (instrument);
		//Debug.Log (charaName);
		//Debug.Log (gender);
		instrumentText.text = instruments[instrument];
        firstName = firstNameInput.text;
        lastName = lastNameInput.text;
        genderToggle();
	}

    // Public Functions
	public void instrumentPlus () {
		instrument += 1;
		if (instrument == instruments.Length) {
			instrument = 0;
		}
	}

	public void instrumentMinus () {
		instrument -= 1;
		if (instrument < 0) {
			instrument = instruments.Length - 1;
		}
	}

	public void playerSave () {
        player = new playerClass.Player(firstName, lastName.Substring(0, 1) + ".", age, gender, instrument, statsFunct.GetComponent<statsFunctions>().GenerateStats(3), 3, 0, 10000);
		playerList.Add(player);
		statsFunct.GetComponent<statsFunctions>().playerJSONSave(playerList, 1);
	}

    // Private Functions
	private void genderToggle () {
		if (genderBoolMale.isOn) {
			gender = 0;
		} else {
			gender = 1;
		}
	}
}