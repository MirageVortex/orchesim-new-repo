﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;

public class statsMenu : MonoBehaviour {

    public int charaIndex;
    public GameObject menu;
	public GameObject characterInfo;

	public GameObject nameText;
    public GameObject genderText;
	public GameObject ageText;
	public GameObject instrumentText;
    //public GameObject musicalityText;
    //public GameObject charismaText;
    //public GameObject diligenceText;
    //public GameObject prestigeText;
    //public GameObject experienceText;

    private List<charaClass.Character> charaList;
    private string[] InstrumentList;
    private string[] GenderList;
    public List<playerClass.Player> playerList;

    // Use this for initialization
    void Start() {
        InstrumentList = characterInfo.GetComponent<itemList>().InstrumentList;
        GenderList = characterInfo.GetComponent<itemList>().GenderList;

        // load NPC stats
        string path = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string jsonString = File.ReadAllText(path);

        // load player stats
        string playerPath = Application.streamingAssetsPath + "/Temp/Player.json";
        string playerJsonString = File.ReadAllText(playerPath);

        charaList = JsonConvert.DeserializeObject<List<charaClass.Character>>(jsonString);
        playerList = JsonConvert.DeserializeObject<List<playerClass.Player>>(playerJsonString);
    }

    void Update() {
        // Player Stats
        UpdateText();

        if (Input.GetKeyDown(KeyCode.Escape)) {
            charaIndex = 0;
        }
    }

    public void OnMouseDown(int NewIndex) {
        charaIndex = NewIndex;
        menu.SetActive(true);
    }

    void EscapeClick() {
        if (!menu.activeSelf) {
            menu.SetActive(true);
        } else {
            menu.SetActive(false);
        }
    }

    void UpdateText() {
        if (charaIndex != 0) {
            nameText.GetComponent<Text>().text = charaList[charaIndex - 1].FirstName.ToString();
            genderText.GetComponent<Text>().text = GenderList[charaList[charaIndex - 1].Gender];
            string age = charaList[charaIndex - 1].Age.ToString() + " Years Old";
            ageText.GetComponent<Text>().text = age;
            instrumentText.GetComponent<Text>().text = InstrumentList[charaList[charaIndex - 1].Instrument];
        } else {
            nameText.GetComponent<Text>().text = playerList[0].FirstName.ToString() + " ";
            genderText.GetComponent<Text>().text = GenderList[charaList[charaIndex - 1].Gender];
            string age = charaList[charaIndex - 1].Age.ToString() + " Years Old";
            ageText.GetComponent<Text>().text = age;
            instrumentText.GetComponent<Text>().text = InstrumentList[charaList[charaIndex - 1].Instrument];
        }

    }
}