﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class radarChart : MonoBehaviour {

	public GameObject L1;
	public GameObject L2;
	public GameObject L3;
	public GameObject L4;
	public GameObject L5;

	public GameObject origin;
	public GameObject V1;
	public GameObject V2;
	public GameObject V3;
	public GameObject V4;
	public GameObject V5;

	public GameObject radarMesh;

	public Material mat;

	private Vector3[] vertices;
	private Vector2[] UV;
	private int[] triangles;

	private GameObject[] linesList;
	private GameObject[] vList;




	// Groups the line objects and vertex objects into their respective arrays --> starts the drawing process
	void Start () {
		linesList = new GameObject[] { L1, L2, L3, L4, L5 };
		vList = new GameObject[] { origin, V1, V2, V3, V4, V5 };
		drawRadarChart ();
	}



	// Rotates the GameObjects by 72 degrees depending on the line order --> Creates general pentagonal shape
	void rotateLines () {
		int counter = 1;
		int rotateZ = 0;
		foreach (GameObject line in linesList) {
			rotateZ = counter * 72;
			line.transform.eulerAngles = new Vector3 (0, 0, rotateZ);
			counter++;
		}
	}



	// Grabs the world position of the modified vList (from rotating the lines) and puts them into its own
	void grabVertices () {
		vertices = new Vector3[6];
		for (int i = 0 ; i < vList.Length ; i++) {
			vertices[i] = new Vector3(vList[i].transform.position.x, vList[i].transform.position.y, vList[i].transform.position.z);
		}
	}


	// Creates UVs based on vertices
	void createUV () {
		UV = new Vector2[6];
		for (int i = 0 ; i < vertices.Length ; i++) {
			UV[i] = new Vector2 (vertices[i].x, vertices[i].y);
		}
	}



	// Creates mesh triangles
	void createTriangles () {
		triangles = new int[] { 0, 0, 0, 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 1 };
	}



	// Draws and adds the mesh component to the mesh GameObject
	void drawRadarChart () {
		Mesh radar = new Mesh ();
		rotateLines ();
		grabVertices ();
		createUV ();
		createTriangles ();
		radar.vertices = vertices;
		radar.uv = UV;
		radar.triangles = triangles;
		radarMesh.GetComponent<MeshFilter> ().mesh = radar;
		radarMesh.AddComponent<MeshRenderer> ().material = mat;
		radarMesh.AddComponent<MeshCollider> ();
	}

}