﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class createNPCs : MonoBehaviour {

    public GameObject itemListObj;
    public GameObject statsFunct;

    private string[] instrumentList;
    private int[] instrumentListCount;

    private List<charaClass.Character> tempCharaList = new List<charaClass.Character>();
    private List<charaClass.Character> CharaList;

    // Use this for initialization
    void Start () {
        // Get the list of game data (names, instruments, etc)
        instrumentList = itemListObj.GetComponent<itemList>().InstrumentList;
        instrumentListCount = itemListObj.GetComponent<itemList>().OrcheInstrumentList;

        // Player chooses instrument
        int playerInstrument = Random.Range(0, 12);

        // Create the new character

        // Remove an instrument slot for the player
        instrumentListCount[playerInstrument]--;

        // Create NPCS randomly
        for (int i = 0; i < instrumentListCount.Length * 2; i++) {
            for (int num = 0; num < instrumentListCount[i]; num++) {
                int firstName = Random.Range(0, 100);
                int lastName = Random.Range(0, 26);
                int Age = Random.Range(27, 51); //Random Age between 27 and 50
                int Gender = Random.Range(0, 2);
                int Rank = statsFunct.GetComponent<statsFunctions>().GetRank();
                int Position = 0;

                tempCharaList.Add(statsFunct.GetComponent<statsFunctions>().CreateNPC(firstName, lastName, Age, Gender, i, Rank, Position));
            }
        }
    }
}
