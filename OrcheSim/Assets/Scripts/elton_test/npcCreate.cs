﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class npcCreate : MonoBehaviour {
    public GameObject itemList;
    public GameObject statsFunct;

    public GameObject emailObject;

    private string[] maleNames;
    private string[] femaleNames;
    private string[] lastNames;

    private int npcNum = 3;

    public List<npcClass.NPC> npcList;

    // Use this for initialization
    void Start() {
        emailObject.GetComponent<emailCreate>().enabled = false;

        maleNames = itemList.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemList.GetComponent<itemList>().FemaleNameList;
        lastNames = itemList.GetComponent<itemList>().LastNameList;

        for (int i = 0; i < npcNum; i++) {
            int gender = Random.Range(0, 2);
            if (gender == 0) {
                npcList.Add(new npcClass.NPC(maleNames[Random.Range(0, 101)], lastNames[Random.Range(0, 26)], i));
            } else {
                npcList.Add(new npcClass.NPC(femaleNames[Random.Range(0, 101)], lastNames[Random.Range(0, 26)], i));
            }
        }
        emailObject.GetComponent<emailCreate>().enabled = true;
    }

    public void npcSave() {
        statsFunct.GetComponent<statsFunctions>().npcJSONSave(npcList);
    }
}
