﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;

public class fullSave : MonoBehaviour {

    public void permSaveFunction() {
        string permCharaSave = Application.streamingAssetsPath + "/Save1/CharaStats.json";
        string permEmailSave = Application.streamingAssetsPath + "/Save1/Email.json";
        string permLibrarySave = Application.streamingAssetsPath + "/Save1/Library.json";
        string permPlayerSave = Application.streamingAssetsPath + "/Save1/Player.json";

        // load temp save files
        string tempCharaSave = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string tempEmailSave = Application.streamingAssetsPath + "/Temp/Email.json";
        string tempLibrarySave = Application.streamingAssetsPath + "/Temp/Library.json";
        string tempPlayerSave = Application.streamingAssetsPath + "/Temp/Player.json";

        // read the perm save files
        string charaSaveString = File.ReadAllText(tempCharaSave);
        string emailSaveString = File.ReadAllText(tempEmailSave);
        string librarySaveString = File.ReadAllText(tempLibrarySave);
        string playerSaveString = File.ReadAllText(tempPlayerSave);

        // write over the new files
        File.WriteAllText(permCharaSave, charaSaveString);
        File.WriteAllText(permEmailSave, emailSaveString);
        File.WriteAllText(permLibrarySave, librarySaveString);
        File.WriteAllText(permPlayerSave, playerSaveString);
    }
}
