﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchMenuTest : MonoBehaviour {

    public GameObject MembersMenu;
    public GameObject LibraryMenu;
    public GameObject EmailMenu;
    public GameObject CalendarMenu;
    public GameObject ExpensesMenu;

    void Start() {
        GameObject[] ManageMenuList = { MembersMenu, LibraryMenu, EmailMenu, CalendarMenu, ExpensesMenu };

        ManageMenuList[0].SetActive(true);
        // Set the rest false
        for (int i = 1; i < 5; i++) {
            ManageMenuList[i].SetActive(false);
        }
    }

    public void SwitchMenu(int menuIndex) {
        GameObject[] ManageMenuList = { MembersMenu, LibraryMenu, EmailMenu, CalendarMenu, ExpensesMenu };

        for (int i=0; i<5; i++) {
            ManageMenuList[i].SetActive(false);
        }

        ManageMenuList[menuIndex].SetActive(true);

    }
}
