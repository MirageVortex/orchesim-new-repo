﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class tempSaveCreate : MonoBehaviour {

	// Use this for initialization
	void Start () {
    }
	
	public void loadSave() {
        // load the perm save files
        string permCharaSave = Application.streamingAssetsPath + "/Save1/CharaStats.json";
        string permEmailSave = Application.streamingAssetsPath + "/Save1/Email.json";
        string permLibrarySave = Application.streamingAssetsPath + "/Save1/Library.json";
        string permPlayerSave = Application.streamingAssetsPath + "/Save1/Player.json";

        // load temp save files
        string tempCharaSave = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string tempEmailSave = Application.streamingAssetsPath + "/Temp/Email.json";
        string tempLibrarySave = Application.streamingAssetsPath + "/Temp/Library.json";
        string tempPlayerSave = Application.streamingAssetsPath + "/Temp/Player.json";

        // read the perm save files
        string charaSaveString = File.ReadAllText(permCharaSave);
        string emailSaveString = File.ReadAllText(permCharaSave);
        string librarySaveString = File.ReadAllText(permCharaSave);
        string playerSaveString = File.ReadAllText(permCharaSave);

        // write over the new files
        File.WriteAllText(tempCharaSave, charaSaveString);
        File.WriteAllText(tempEmailSave, emailSaveString);
        File.WriteAllText(tempLibrarySave, librarySaveString);
        File.WriteAllText(tempPlayerSave, playerSaveString);
    }
}
