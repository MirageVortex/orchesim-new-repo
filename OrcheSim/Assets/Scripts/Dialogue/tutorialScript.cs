﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialScript : MonoBehaviour {

	// Sets description dialogue
	void Awake () {
		string bedTutorial = "For sleeping, Resting will auto-save";
		string computerTutorial = "For management. Using it will spend some time in the day.";
		string doorTutorial = "To the overworld. Exiting will advance the time of day.";
	}
}
