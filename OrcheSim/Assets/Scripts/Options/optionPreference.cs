﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class optionPreference : MonoBehaviour {

    Slider slide;
    public Slider slider;

	// Use this for initialization
	void Start () {
        slide = slider.GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeMainValue()
    {
        PlayerPrefs.SetFloat("volumeMain", slide.value);
    }

    public void ChangeMusicValue()
    {
        PlayerPrefs.SetFloat("volumeMusic", slide.value);
    }

    public void ChangeSoundValue()
    {
        PlayerPrefs.SetFloat("volumeSound", slide.value);
    }

    public void ChangeConcertValue()
    {
        PlayerPrefs.SetFloat("volumeConcert", slide.value);
    }

    public void SaveChanges()
    {
        PlayerPrefs.Save();
    }

    public void LoadLevel()
    {
        SaveChanges();
        SceneManager.LoadScene("titlescreen");
    }

}
