﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGame : MonoBehaviour {

	public void SavePosition() {
        PlayerPrefs.SetFloat("PositionX", transform.position.x);
        PlayerPrefs.SetFloat("PositionY", transform.position.y);
        PlayerPrefs.SetFloat("PositionZ", transform.position.z);
    }

    public void LoadPosition() {
        float x = PlayerPrefs.GetFloat("PositionX");
        float y = PlayerPrefs.GetFloat("PositionY");
        float z = PlayerPrefs.GetFloat("PositionZ");

        transform.position = new Vector3(x, y, z);
    }
}
