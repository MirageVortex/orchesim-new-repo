﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

[System.Serializable]
public class npcTest {
    public string npcName;
    public string npcInstrument;
    public int rank;
}

public class npcScrollList : MonoBehaviour {

    public GameObject itemListObj;

    string[] maleNames;
    string[] femaleNames;
    string[] lastNames;
    string[] genderList;
    string[] instrumentList;
    int[] instrumentListCount;

    public List<charaClass.Character> npcList;
    public Transform contentPanel;
    // public npcScrollList otherNPCList;

    public SimpleObjectPool buttonObjectPool;

	// Use this for initialization
	void Start () {
        maleNames = itemListObj.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemListObj.GetComponent<itemList>().FemaleNameList;
        lastNames = itemListObj.GetComponent<itemList>().LastNameList;
        genderList = itemListObj.GetComponent<itemList>().GenderList;
        instrumentList = itemListObj.GetComponent<itemList>().InstrumentList;
        instrumentListCount = itemListObj.GetComponent<itemList>().OrcheInstrumentList;

        // Load in current playerList
        string path = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string jsonString = File.ReadAllText(path);

        npcList = JsonConvert.DeserializeObject<List<charaClass.Character>>(jsonString);

        RefreshDisplay();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void RefreshDisplay() {
        AddButtons();
    }

    private void AddButtons() {
        for (int i=0; i < npcList.Count; i++) {
            charaClass.Character npc = npcList[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel, false);

            updateButtonElements sampleButton = newButton.GetComponent<updateButtonElements>();
            sampleButton.Setup(npc, instrumentList[npc.Instrument], this);
        }

    }
}
