﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class updateButtonElements : MonoBehaviour {
    public Button button;
    public Text nameText;
    public Text instrumentText;
    public Text rankText;

    private charaClass.Character npc;
    private npcScrollList scrollList;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Setup(charaClass.Character currentNPC, string instrument, npcScrollList currentScrollList) {
        npc = currentNPC;
        nameText.text = npc.FirstName + ' ' + npc.LastName;
        instrumentText.text = instrument;
        rankText.text = npc.Rank.ToString();

        scrollList = currentScrollList;
    }
}
